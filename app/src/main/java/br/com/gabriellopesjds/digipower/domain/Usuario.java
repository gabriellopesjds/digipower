package br.com.gabriellopesjds.digipower.domain;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.UUID;

/**
 * Created by gabri on 24/02/2018.
 */

public class Usuario implements Parcelable {
    private String idCliente;
    private String nome;
    private String email;
    private String senha;
    private String liberacao;

    public Usuario(String idCliente, String nome, String email, String senha, String liberacao) {
        this.idCliente = idCliente;
        this.nome = nome;
        this.email = email;
        this.senha = senha;
        this.liberacao = liberacao;
    }

    public Usuario(String nome, String email, String senha) {
        this.idCliente = UUID.randomUUID().toString();
        this.liberacao = UUID.randomUUID().toString().substring(0,4);
        this.nome = nome;
        this.email = email;
        this.senha = senha;
    }

    public Usuario() {

    }

    protected Usuario(Parcel in) {
        idCliente = in.readString();
        nome = in.readString();
        email = in.readString();
        senha = in.readString();
        liberacao = in.readString();
    }

    public static final Creator<Usuario> CREATOR = new Creator<Usuario>() {
        @Override
        public Usuario createFromParcel(Parcel in) {
            return new Usuario(in);
        }

        @Override
        public Usuario[] newArray(int size) {
            return new Usuario[size];
        }
    };

    public String getIdCliente() {
        return idCliente;
    }

    public String getNome() {
        return nome;
    }

    public String getEmail() {
        return email;
    }

    public String getSenha() {
        return senha;
    }

    public void setIdCliente(String idCliente) {
        this.idCliente = idCliente;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getLiberacao() {
        return liberacao;
    }

    public void setLiberacao(String liberacao) {
        this.liberacao = liberacao;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(idCliente);
        dest.writeString(nome);
        dest.writeString(email);
        dest.writeString(senha);
        dest.writeString(liberacao);
    }
}

