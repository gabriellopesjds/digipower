package br.com.gabriellopesjds.digipower.mvp;

import android.os.Bundle;
import android.widget.EditText;

import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.UUID;

import br.com.gabriellopesjds.digipower.domain.Usuario;
import br.com.gabriellopesjds.digipower.interfaces.ObserverCliente;
import br.com.gabriellopesjds.digipower.util.TarefaGravarCadastroCliente;

public class ModelCadastro implements MVP.ModelCadastro {
    private MVP.PresenterCadastro presenter;

    public ModelCadastro( MVP.PresenterCadastro presenter ){
        this.presenter = presenter;
    }

    @Override
    public void callServiceGravarCadastro(EditText etNomeCadastro, EditText etEmailCadastro, EditText etSenhaCadastro) {
        Usuario usuario = new Usuario(etNomeCadastro.getText().toString(), etEmailCadastro.getText().toString(), etSenhaCadastro.getText().toString());
        if (!presenter.validarSeEmailJaExiste(etEmailCadastro)) {
            TarefaGravarCadastroCliente tarefaGravarCadastroCliente = new TarefaGravarCadastroCliente(usuario, presenter.getContext(), (ObserverCliente) presenter.getContext());
            tarefaGravarCadastroCliente.execute();
        } else {
            presenter.chamaDialogEmailExistente(usuario);
        }
    }
}
