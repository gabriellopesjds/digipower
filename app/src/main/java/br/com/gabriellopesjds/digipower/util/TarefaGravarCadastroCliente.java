package br.com.gabriellopesjds.digipower.util;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import br.com.gabriellopesjds.digipower.database.UsuarioDAO;
import br.com.gabriellopesjds.digipower.domain.Usuario;
import br.com.gabriellopesjds.digipower.interfaces.ObserverCliente;

public class TarefaGravarCadastroCliente extends AsyncTask<Void, Void, Void> {
    private Context context;
    private Usuario usuario;
    private ProgressDialog progress;
    private ObserverCliente observerCliente;


    public TarefaGravarCadastroCliente(Usuario usuario, Context context, ObserverCliente observerCliente) {
        this.usuario = usuario;
        this.context = context;
        this.observerCliente = observerCliente;
    }

    @Override
    protected void onPreExecute() {
        progress = new ProgressDialog(context);
        progress.setMessage("Gravando cadastro ...");
        progress.setCancelable(false);
        progress.show();
    }

    @Override
    protected Void doInBackground(Void... params) {
        UsuarioDAO dao = new UsuarioDAO(context);
        dao.addUsuario(usuario);
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        progress.dismiss();
        observerCliente.chamarIntentTelaPrincipal(usuario);
    }
}