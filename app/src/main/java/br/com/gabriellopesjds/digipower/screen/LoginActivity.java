package br.com.gabriellopesjds.digipower.screen;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;

import br.com.gabriellopesjds.digipower.R;
import br.com.gabriellopesjds.digipower.domain.Usuario;
import br.com.gabriellopesjds.digipower.mvp.MVP;
import br.com.gabriellopesjds.digipower.mvp.PresenterLogin;

public class LoginActivity extends AppCompatActivity implements MVP.ViewLogin {
    private AlertDialog alertDialog;
    private EditText etEmail,etSenha;
    private Button btnEntrar;
    private TextView etEsqueciMinhaSenha;
    private static CheckBox show_hide_password;
    private TextInputLayout ilEmail;
    private MVP.PresenterLogin presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        if( presenter == null ){
            presenter = new PresenterLogin();
        }
        presenter.setView(this);
        initView();
        setListener();
        setTextChangedListener();
    }

    public void initView() {
        etEmail = (EditText) findViewById(R.id.et_email);
        etSenha = (EditText) findViewById(R.id.et_senha);
        btnEntrar = (Button) findViewById(R.id.btn_entrar);
        ilEmail = (TextInputLayout) findViewById(R.id.input_layout_email);
        etEsqueciMinhaSenha = (TextView) findViewById(R.id.tv_esqueci_senha);
        show_hide_password = (CheckBox) findViewById(R.id.show_hide_password);
    }

    public void setListener() {
        show_hide_password
                .setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

                    @Override
                    public void onCheckedChanged(CompoundButton button,
                                                 boolean isChecked) {
                        if (isChecked) {
                            show_hide_password.setText(R.string.message_ocultar_senha);// change

                            etSenha.setInputType(InputType.TYPE_CLASS_TEXT);
                            etSenha.setTransformationMethod(HideReturnsTransformationMethod
                                    .getInstance());// show password
                        } else {
                            show_hide_password.setText(R.string.message_mostrar_senha);// change

                            etSenha.setInputType(InputType.TYPE_CLASS_TEXT
                                    | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                            etSenha.setTransformationMethod(PasswordTransformationMethod
                                    .getInstance());// hide password
                        }
                    }
                });

        etEmail.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
                return false;
            }
        });

        etSenha.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    presenter.validarEmailSenha(etEmail, etSenha);
                }
                return false;
            }
        });

        btnEntrar.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.validarEmailSenha(etEmail, etSenha);
            }
        });

        etEsqueciMinhaSenha.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                chamaDialogEsqueciMinhaSenha();
            }
        });
    }

    private void setTextChangedListener() {
        etEmail.addTextChangedListener(new ObservadorTextWatcher(etEmail));
    }

    @Override
    public void showSnackbar(String mensagem) {
        Snackbar.make(getCurrentFocus(), mensagem, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void setTextInputLayout(TextInputLayout textInputLayout, String mensagem) {
        textInputLayout.setError(mensagem);
    }

    @Override
    public void setDrawableEditText(EditText editText, int left, int top, int right, int bottom) {
        editText.setCompoundDrawablesWithIntrinsicBounds(left,top,right,bottom);
    }


    @Override
    public void setErrorEnabled(TextInputLayout textInputLayout, boolean error) {
        textInputLayout.setErrorEnabled(error);
    }

    @Override
    public void usuarioValidado(Usuario usuario) {
        Intent intent = new Intent(this, ControleActivity.class);
        intent.putExtra("USUARIO", usuario);
        startActivity(intent);
    }

    private class ObservadorTextWatcher implements TextWatcher {
        private View view;

        private ObservadorTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.et_email:
                    presenter.validarEmail(ilEmail, etEmail);
                    break;
            }
        }
    }


    public void chamaDialogEsqueciMinhaSenha() {
        LayoutInflater inflater = getLayoutInflater();
        View view = inflater.inflate(R.layout.item_dialog_esqueci_senha, null);
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        AutoCompleteTextView email = (AutoCompleteTextView) view.findViewById(R.id.et_email_esqueci_senha);
        email.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    alertDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
                }
            }
        });

        builder.setPositiveButton(R.string.dialog_confirmar, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        builder.setNegativeButton(R.string.dialog_cancelar, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                alertDialog.dismiss();
            }
        });
        builder.setView(view);
        alertDialog = builder.create();
        alertDialog.show();
    }
}