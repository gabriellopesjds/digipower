package br.com.gabriellopesjds.digipower.mvp;

import android.content.Context;
import android.support.annotation.DrawableRes;
import android.support.design.widget.TextInputLayout;
import android.view.View;
import android.widget.EditText;

import com.facebook.login.LoginResult;

import br.com.gabriellopesjds.digipower.domain.Usuario;

public interface MVP {


    interface PresenterCadastro {
        public Context getContext();

        public void showSnackbar(String mensagem);

        public void setViewCadastro(ViewCadastro viewCadastro);

        public boolean emailEhValido(String email);

        public boolean senhaEhIgual(String senha, String confirmaSenha);

        public boolean validarSeCamposEstaoPreenchidos(EditText nome, EditText email, EditText senha, EditText senhaConfirma);

        public boolean validarEmail(TextInputLayout inputLayoutEmail, EditText inputEmail);

        public boolean validarSenha(TextInputLayout inputLayoutSenha, EditText editTextSenha);

        public boolean validarConfirmarSenha(TextInputLayout ilSenhaCadastro, EditText etSenhaCadastro, TextInputLayout ilSenhaConfirmaCadastro, EditText etSenhaConfirmaCadastro);

        public boolean validarNome(TextInputLayout ilNomeCadastro, EditText etNomeCadastro);

        public boolean validarSeEmailJaExiste(EditText email);

        public boolean senhaEhValida(String senha);

        public void callServiceGravarCadastro(EditText etNomeCadastro, EditText etEmailCadastro, EditText etSenhaCadastro);

        public void chamaDialogEmailExistente(final Usuario usuario);
    }

    interface ViewCadastro {
        public void requestFocus(View view);

        public void showSnackbar(String mensagem);

        public void setTextInputLayout(TextInputLayout textInputLayout, String mensagem);

        public void setDrawableEditText(EditText editText, @DrawableRes int left, @DrawableRes int top, @DrawableRes int right, @DrawableRes int bottom);

        public void setErrorEnabled(TextInputLayout textInputLayout, boolean error);

        public void usuarioValidado(Usuario usuario);

        public void chamaDialogEmailExistente(final Usuario usuario);
    }

    interface ModelCadastro{
        public void callServiceGravarCadastro(EditText etNomeCadastro, EditText etEmailCadastro, EditText etSenhaCadastro);
    }

    interface PresenterLogin {
        public void setView(ViewLogin view);

        public boolean validarEmail(TextInputLayout inputLayoutEmail, EditText inputEmail);

        public void validarEmailSenha(EditText usuarioDigitado, EditText senhaDigitada);

        public boolean validarSeCamposEstaoPreenchidos(EditText email, EditText senha);

        public boolean emailEhValido(String email);

        public Context getContext();
    }

    interface ViewLogin {
        public void showSnackbar(String mensagem);

        public void setTextInputLayout(TextInputLayout textInputLayout, String mensagem);

        public void setDrawableEditText(EditText editText, @DrawableRes int left, @DrawableRes int top, @DrawableRes int right, @DrawableRes int bottom);

        public void setErrorEnabled(TextInputLayout textInputLayout, boolean error);

        public void usuarioValidado(Usuario usuario);
    }


    interface PresenterMain {
        public boolean validarSeEmailJaExiste(String email);

        public void setView(ViewMain view);

        public Context getContext();

        public void fazerLoginComFacebook(LoginResult loginResult);

        public void chamarIntentTelaPrincipal(Usuario usuario);

    }

    interface ViewMain {
        public void showSnackbar(String mensagem);

        public void chamarIntentTelaPrincipal(Usuario usuario);
    }

    interface ModelMain {
        public void fazerLoginComFacebook(LoginResult loginResult);
    }

    interface PresenterControle {
        public void setView(ViewControle view);

        public Context getContext();
    }

    interface ViewControle {
        public void showSnackbar(String mensagem);
    }


}
