package br.com.gabriellopesjds.digipower.screen;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.SeekBar;

import com.facebook.login.LoginManager;

import br.com.gabriellopesjds.digipower.R;
import br.com.gabriellopesjds.digipower.domain.Usuario;
import br.com.gabriellopesjds.digipower.mvp.MVP;
import br.com.gabriellopesjds.digipower.mvp.PresenterControle;

public class ControleActivity extends AppCompatActivity implements MVP.ViewControle{
    private SeekBar seekBar;
    private ImageView ivDown, ivUp, ivConfiguracao;
    private Dialog mAlertDialog;
    private Usuario usuario;
    private MVP.PresenterControle presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_controle);

        if(presenter == null){
            presenter = new PresenterControle();
        }
        presenter.setView(this);

        Bundle bundle = getIntent().getExtras();
        if (bundle.getParcelable("USUARIO") != null) {
            usuario = bundle.getParcelable("USUARIO");
            showSnackbar("OLÁ " + usuario.getNome().toUpperCase() + ", SEJA BEM VINDO ! ");
        }
        initView();
        setListener();
    }

    private void initView() {
        seekBar = (SeekBar) findViewById(R.id.seekBar);
        ivDown = (ImageView) findViewById(R.id.iv_down);
        ivUp = (ImageView) findViewById(R.id.iv_up);
        ivUp.setVisibility(ImageView.INVISIBLE);
        ivDown.setVisibility(ImageView.INVISIBLE);
        ivConfiguracao = (ImageView) findViewById(R.id.iv_configuracao);
    }

    private void setListener() {
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int progresso = seekBar.getProgress();

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (progress > progresso) {
                    ivUp.setVisibility(ImageView.VISIBLE);
                    ivDown.setVisibility(ImageView.INVISIBLE);
                } else {
                    ivDown.setVisibility(ImageView.VISIBLE);
                    ivUp.setVisibility(ImageView.INVISIBLE);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                ivUp.setVisibility(ImageView.INVISIBLE);
                ivDown.setVisibility(ImageView.INVISIBLE);
            }
        });

        ivConfiguracao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chamarIntentTelaConfiguracao();
            }
        });
    }

    public void chamarIntentTelaConfiguracao() {
        Intent intent = new Intent(this, ConfiguracaoActivity.class);
        intent.putExtra("USUARIO", usuario);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        final AlertDialog.Builder mBuilder = new AlertDialog.Builder(this);
        mBuilder.setTitle(R.string.title_sair_app);
        mBuilder.setNegativeButton(R.string.dialog_cancelar, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mAlertDialog.dismiss();
            }
        });
        mBuilder.setPositiveButton(R.string.dialog_confirmar, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                LoginManager.getInstance().logOut();
                mAlertDialog.dismiss();
                finish();
            }
        });
        mAlertDialog = mBuilder.create();
        mAlertDialog.show();
    }

    @Override
    public void showSnackbar(String mensagem) {
        Snackbar.make(getWindow().getDecorView().getRootView(), mensagem, Snackbar.LENGTH_LONG).show();
    }

}