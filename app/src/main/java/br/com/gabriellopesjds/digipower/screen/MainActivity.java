package br.com.gabriellopesjds.digipower.screen;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import java.util.Arrays;

import br.com.gabriellopesjds.digipower.interfaces.ObserverCliente;
import br.com.gabriellopesjds.digipower.R;
import br.com.gabriellopesjds.digipower.database.CriarBancoSqlite;
import br.com.gabriellopesjds.digipower.domain.Usuario;
import br.com.gabriellopesjds.digipower.mvp.MVP;
import br.com.gabriellopesjds.digipower.mvp.PresenterMain;

public class MainActivity extends AppCompatActivity implements MVP.ViewMain, ObserverCliente {
    private LoginButton btnLoginFacebook;
    private CallbackManager callbackManager;
    public static SQLiteDatabase banco;
    private MVP.PresenterMain presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (presenter == null) {
            presenter = new PresenterMain();
        }
        presenter.setView(this);

        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().logOut();

        initView();

        //CRIANDO DATABASE SQLITE.
        CriarBancoSqlite db = new CriarBancoSqlite(this);
        banco = db.getWritableDatabase();

        btnLoginFacebook.setReadPermissions(Arrays.asList("public_profile", "email"));

        btnLoginFacebook.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                presenter.fazerLoginComFacebook(loginResult);
            }

            @Override
            public void onCancel() {
                showSnackbar("Atenção, operação cancelada!");
            }

            @Override
            public void onError(FacebookException exception) {
                showSnackbar("Atenção, não foi possível conectar!");
            }
        });
    }

    public void initView() {
        btnLoginFacebook = (LoginButton) findViewById(R.id.btn_login_facebook);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void chamarIntentActivityDeCadastro(View view) {
        Intent intent = new Intent(this, CadastroActivity.class);
        startActivity(intent);
    }

    public void chamarIntentActivityDeLogin(View view) {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }

    @Override
    public void showSnackbar(String mensagem) {
        Snackbar.make(getWindow().getDecorView().getRootView(), mensagem, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void chamarIntentTelaPrincipal(Usuario usuario) {
        Intent intent = new Intent(this, ControleActivity.class);
        intent.putExtra("USUARIO", usuario);
        startActivity(intent);
    }
}