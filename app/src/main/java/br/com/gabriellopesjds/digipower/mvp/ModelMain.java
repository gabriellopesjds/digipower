package br.com.gabriellopesjds.digipower.mvp;

import android.os.Bundle;

import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.UUID;

import br.com.gabriellopesjds.digipower.interfaces.ObserverCliente;
import br.com.gabriellopesjds.digipower.util.TarefaGravarCadastroCliente;
import br.com.gabriellopesjds.digipower.domain.Usuario;

public class ModelMain implements MVP.ModelMain {
    private MVP.PresenterMain presenter;

    public ModelMain( MVP.PresenterMain presenter ){
        this.presenter = presenter;
    }

    @Override
    public void fazerLoginComFacebook(LoginResult loginResult) {
        GraphRequest request = GraphRequest.newMeRequest(
                loginResult.getAccessToken(),
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        try {
                            Usuario usuario;
                            String nome = object.getString("name");
                            String email = object.getString("email");
                            UUID uuid = UUID.randomUUID();
                            String senha = uuid.toString().substring(0, 6);
                            usuario = new Usuario(nome, email, senha);
                            if (!presenter.validarSeEmailJaExiste(email)) {
                                TarefaGravarCadastroCliente tarefaGravarCadastroCliente = new TarefaGravarCadastroCliente(usuario, presenter.getContext(), (ObserverCliente) presenter.getContext());
                                tarefaGravarCadastroCliente.execute();
                            } else {
                                presenter.chamarIntentTelaPrincipal(usuario);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,email,gender,birthday");
        request.setParameters(parameters);
        request.executeAsync();
    }
}
