package br.com.gabriellopesjds.digipower.mvp;

import android.content.Context;
import android.support.design.widget.TextInputLayout;
import android.text.TextUtils;
import android.widget.EditText;

import br.com.gabriellopesjds.digipower.R;
import br.com.gabriellopesjds.digipower.database.UsuarioDAO;
import br.com.gabriellopesjds.digipower.domain.Usuario;
import br.com.gabriellopesjds.digipower.util.TarefaGravarCadastroCliente;

public class PresenterCadastro implements MVP.PresenterCadastro {
    private MVP.ViewCadastro viewCadastro;
    private MVP.ModelCadastro modelCadastro;

    public PresenterCadastro (){
        this.modelCadastro = new ModelCadastro(this);
    }

    @Override
    public Context getContext() {
        return (Context) viewCadastro;
    }

    @Override
    public void showSnackbar(String mensagem) {
        viewCadastro.showSnackbar(mensagem);
    }

    @Override
    public void setViewCadastro(MVP.ViewCadastro viewCadastro) {
        this.viewCadastro = viewCadastro;
    }

    @Override
    public boolean validarSeCamposEstaoPreenchidos(EditText nome, EditText email, EditText senha, EditText senhaConfirma) {
        if (nome.getText().toString().trim().isEmpty() || email.getText().toString().trim().isEmpty() || senha.getText().toString().trim().isEmpty() || senhaConfirma.getText().toString().trim().isEmpty()) {
            viewCadastro.showSnackbar("Todos os campos são obrigatórios !");
            return false;
        }
        return true;
    }

    @Override
    public boolean emailEhValido(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    @Override
    public boolean senhaEhIgual(String senha, String confirmaSenha) {
        return senha.equals(confirmaSenha);
    }

    @Override
    public boolean validarEmail(TextInputLayout inputLayoutEmail, EditText inputEmail) {
        String email = inputEmail.getText().toString().trim();
        if (email.isEmpty() || !emailEhValido(email)) {
            viewCadastro.setTextInputLayout(inputLayoutEmail, "E-mail inválido");
            viewCadastro.setDrawableEditText(inputEmail, R.drawable.ic_email, 0, R.drawable.ic_close, 0);
            viewCadastro.requestFocus(inputLayoutEmail);
            return false;
        } else {
            viewCadastro.setErrorEnabled(inputLayoutEmail, false);
            viewCadastro.setDrawableEditText(inputEmail, R.drawable.ic_email, 0, R.drawable.ic_check, 0);
        }
        return true;
    }

    @Override
    public boolean validarSenha(TextInputLayout inputLayoutSenha, EditText editTextSenha) {
        String senha = editTextSenha.getText().toString();
        if (TextUtils.isEmpty(senha) || !senhaEhValida(senha)) {
            viewCadastro.setDrawableEditText(editTextSenha, R.drawable.ic_senha, 0, R.drawable.ic_close, 0);
            viewCadastro.setTextInputLayout(inputLayoutSenha, "A senha deve conter mais de 5 caracteres.");
            viewCadastro.requestFocus(inputLayoutSenha);
            return false;
        } else {
            viewCadastro.setDrawableEditText(editTextSenha, R.drawable.ic_senha, 0, R.drawable.ic_check, 0);
            viewCadastro.setErrorEnabled(inputLayoutSenha, false);
        }
        return true;
    }

    @Override
    public boolean validarConfirmarSenha(TextInputLayout ilSenhaCadastro, EditText etSenhaCadastro, TextInputLayout ilSenhaConfirmaCadastro, EditText etSenhaConfirmaCadastro) {
        String senha = etSenhaCadastro.getText().toString();
        String confirmaSenha = etSenhaConfirmaCadastro.getText().toString();
        if (!senhaEhIgual(senha, confirmaSenha)) {
            viewCadastro.setTextInputLayout(ilSenhaConfirmaCadastro, "A senha está diferente.");
            viewCadastro.setDrawableEditText(etSenhaConfirmaCadastro, R.drawable.ic_senha, 0, R.drawable.ic_close, 0);
            viewCadastro.requestFocus(ilSenhaConfirmaCadastro);
            return false;
        } else {
            viewCadastro.setErrorEnabled(ilSenhaConfirmaCadastro, false);
            viewCadastro.setDrawableEditText(etSenhaConfirmaCadastro, R.drawable.ic_senha, 0, R.drawable.ic_check, 0);
        }
        return true;
    }

    @Override
    public boolean validarNome(TextInputLayout ilNomeCadastro, EditText etNomeCadastro) {
        String nome = etNomeCadastro.getText().toString();
        if (nome.trim().isEmpty()) {
            viewCadastro.setTextInputLayout(ilNomeCadastro, "Necessário informar o nome!");
            viewCadastro.setDrawableEditText(etNomeCadastro, R.drawable.ic_personagem, 0, R.drawable.ic_close, 0);
            viewCadastro.requestFocus(ilNomeCadastro);
            return false;
        } else {
            viewCadastro.setErrorEnabled(ilNomeCadastro, false);
            viewCadastro.setDrawableEditText(etNomeCadastro, R.drawable.ic_personagem, 0, R.drawable.ic_check, 0);
        }
        return true;
    }

    @Override
    public boolean validarSeEmailJaExiste(EditText email) {
        if (!new UsuarioDAO(getContext()).checkUsuario(email.getText().toString())) {
            return false;
        }
        return true;
    }

    @Override
    public boolean senhaEhValida(String senha) {
        return senha.length() >= 6;
    }

    @Override
    public void callServiceGravarCadastro(EditText etNomeCadastro, EditText etEmailCadastro, EditText etSenhaCadastro) {
        modelCadastro.callServiceGravarCadastro(etNomeCadastro, etEmailCadastro, etSenhaCadastro);
    }

    @Override
    public void chamaDialogEmailExistente(Usuario usuario) {
        viewCadastro.chamaDialogEmailExistente(usuario);
    }
}