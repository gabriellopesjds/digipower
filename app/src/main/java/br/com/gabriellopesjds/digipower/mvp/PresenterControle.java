package br.com.gabriellopesjds.digipower.mvp;

import android.content.Context;

import br.com.gabriellopesjds.digipower.database.UsuarioDAO;

public class PresenterControle implements MVP.PresenterControle{
    private MVP.ViewControle view;

    @Override
    public void setView(MVP.ViewControle view) {
        this.view = view;
    }

    @Override
    public Context getContext() {
        return (Context) view;
    }
}
