package br.com.gabriellopesjds.digipower.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class CriarBancoSqlite extends SQLiteOpenHelper implements Tables {
    private static final String NOME_DO_BANCO = "db_digipower";
    private static int VERSAO_DO_BANCO = 1;

    //CRIAR TABELA DO USUARIO
    private String CREATE_USUARIO_TABLE = "CREATE TABLE " + TABLE_USUARIO + "("
            + COLUMN_USUARIO_ID + " VARCHAR(36) PRIMARY KEY," + COLUMN_USUARIO_NOME + " TEXT,"
            + COLUMN_USUARIO_EMAIL+ " TEXT," + COLUMN_USUARIO_SENHA + " TEXT," + COLUMN_USUARIO_LIBERACAO + " VARCHAR(4))";

    //DELETAR TABELA DO USUARIO
    private String DROP_USER_TABLE = "DROP TABLE IF EXISTS " + TABLE_USUARIO;


    public CriarBancoSqlite(Context context) {
        super(context, NOME_DO_BANCO, null, VERSAO_DO_BANCO);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_USUARIO_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(DROP_USER_TABLE);
        onCreate(db);
    }
}