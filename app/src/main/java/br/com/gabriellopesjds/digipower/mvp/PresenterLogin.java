package br.com.gabriellopesjds.digipower.mvp;

import android.content.Context;
import android.support.design.widget.TextInputLayout;
import android.text.TextUtils;
import android.widget.EditText;

import br.com.gabriellopesjds.digipower.R;
import br.com.gabriellopesjds.digipower.database.UsuarioDAO;
import br.com.gabriellopesjds.digipower.domain.Usuario;

public class PresenterLogin implements MVP.PresenterLogin {
    private MVP.ViewLogin view;

    @Override
    public Context getContext() {
        return (Context) view;
    }

    @Override
    public void setView(MVP.ViewLogin view) {
        this.view = view;
    }

    @Override
    public boolean validarEmail(TextInputLayout inputLayoutEmail, EditText inputEmail) {
        String email = inputEmail.getText().toString().trim();
        if (email.isEmpty() || !emailEhValido(email)) {
            view.setTextInputLayout(inputLayoutEmail, "E-mail inválido");
            view.setDrawableEditText(inputEmail, R.drawable.ic_email, 0, R.drawable.ic_close, 0);
            return false;
        } else {
            view.setErrorEnabled(inputLayoutEmail, false);
            view.setDrawableEditText(inputEmail, R.drawable.ic_email, 0, R.drawable.ic_check, 0);
        }
        return true;
    }

    @Override
    public void validarEmailSenha(EditText usuarioDigitado, EditText senhaDigitada) {
        if (validarSeCamposEstaoPreenchidos(usuarioDigitado, senhaDigitada)) {
            if (new UsuarioDAO(getContext()).checkUsuario(usuarioDigitado.getText().toString().trim()
                    , senhaDigitada.getText().toString().trim())) {
                Usuario user = new UsuarioDAO(getContext()).getUsuarioByEmail(usuarioDigitado.getText().toString().trim());
                view.usuarioValidado(user);
            } else {
                view.showSnackbar("E-mail ou senha inválido !");
            }
        }
    }

    @Override
    public boolean validarSeCamposEstaoPreenchidos(EditText email, EditText senha) {
        if (email.getText().toString().trim().isEmpty() && senha.getText().toString().trim().isEmpty()) {
            view.showSnackbar("Atenção, o email e senha são obrigatórios !");
            return false;
        }
        return true;
    }

    @Override
    public boolean emailEhValido(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }
}
