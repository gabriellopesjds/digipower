package br.com.gabriellopesjds.digipower.screen;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.login.LoginManager;

import br.com.gabriellopesjds.digipower.BuildConfig;
import br.com.gabriellopesjds.digipower.R;
import br.com.gabriellopesjds.digipower.domain.Usuario;

public class ConfiguracaoActivity extends AppCompatActivity {
    private ImageView ivVoltar;
    private Button btnSair;
    private Dialog mAlertDialog;
    private TextView tvLiberacao, tvVersao;
    private Usuario usuario;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configuracao);
        Bundle bundle = getIntent().getExtras();
        if (bundle.getParcelable("USUARIO") != null) {
            usuario = bundle.getParcelable("USUARIO");
        }
        initView();
        setListener();
    }

    private void initView() {
        ivVoltar = (ImageView) findViewById(R.id.iv_voltar);
        btnSair = (Button) findViewById(R.id.btn_sair);
        tvLiberacao = (TextView) findViewById(R.id.tv_codigo_liberacao);
        tvLiberacao.setText(usuario.getLiberacao());
        tvVersao = (TextView) findViewById(R.id.tv_versao);
        tvVersao.setText(BuildConfig.VERSION_NAME);
    }

    private void setListener() {
        ivVoltar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        btnSair.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chamarDialogSair();
            }
        });
    }

    public void chamarDialogSair() {
        final AlertDialog.Builder mBuilder = new AlertDialog.Builder(this);
        mBuilder.setTitle(R.string.title_sair_app);
        mBuilder.setNegativeButton(R.string.dialog_cancelar, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mAlertDialog.dismiss();
            }
        });
        mBuilder.setPositiveButton(R.string.dialog_confirmar, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                LoginManager.getInstance().logOut();
                finishAffinity();
                chamarIntentTelaInicial();
            }
        });
        mAlertDialog = mBuilder.create();
        mAlertDialog.show();
    }


    public void chamarIntentTelaInicial() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}