package br.com.gabriellopesjds.digipower.screen;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import br.com.gabriellopesjds.digipower.interfaces.ObserverCliente;
import br.com.gabriellopesjds.digipower.R;
import br.com.gabriellopesjds.digipower.util.TarefaGravarCadastroCliente;
import br.com.gabriellopesjds.digipower.database.UsuarioDAO;
import br.com.gabriellopesjds.digipower.domain.Usuario;
import br.com.gabriellopesjds.digipower.mvp.MVP;
import br.com.gabriellopesjds.digipower.mvp.PresenterCadastro;

public class CadastroActivity extends AppCompatActivity implements MVP.ViewCadastro, ObserverCliente {
    private AlertDialog alertDialog;
    private EditText etNomeCadastro,etSenhaCadastro,etSenhaConfirmaCadastro;
    private TextInputLayout ilNomeCadastro,ilEmailCadastro,ilSenhaCadastro,ilSenhaConfirmaCadastro;
    private AutoCompleteTextView etEmailCadastro;
    private Button btnCriarConta;
    private Dialog mAlertDialog;
    private static MVP.PresenterCadastro presenter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro);

        if (presenter == null) {
            presenter = new PresenterCadastro();
        }
        presenter.setViewCadastro(this);
        initView();
        setListener();
        setTextChangedListener();
    }

    public void initView() {
        etNomeCadastro = (EditText) findViewById(R.id.et_nome_cadastro);
        etEmailCadastro = (AutoCompleteTextView) findViewById(R.id.et_email_cadastro);
        etSenhaCadastro = (EditText) findViewById(R.id.et_senha_cadastro);
        etSenhaConfirmaCadastro = (EditText) findViewById(R.id.et_senha_confirma_cadastro);
        ilNomeCadastro = (TextInputLayout) findViewById(R.id.il_nome_cadastro);
        ilEmailCadastro = (TextInputLayout) findViewById(R.id.il_email_cadastro);
        ilSenhaCadastro = (TextInputLayout) findViewById(R.id.il_senha_cadastro);
        ilSenhaConfirmaCadastro = (TextInputLayout) findViewById(R.id.il_senha_confirma_cadastro);
        btnCriarConta = (Button) findViewById(R.id.btn_criar_conta);
    }

    public void setListener() {
        etSenhaConfirmaCadastro.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    submitForm();
                }
                return false;
            }
        });

        btnCriarConta.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                submitForm();
            }
        });
    }

    public void submitForm() {
        if (!presenter.validarSeCamposEstaoPreenchidos(etNomeCadastro, etEmailCadastro, etSenhaCadastro, etSenhaConfirmaCadastro)) {
            return;
        }
        if (!presenter.validarNome(ilNomeCadastro, etNomeCadastro)) {
            return;
        }
        if (!presenter.validarEmail(ilEmailCadastro, etEmailCadastro)) {
            return;
        }
        if (!presenter.validarSenha(ilSenhaCadastro, etSenhaCadastro)) {
            return;
        }
        if (!presenter.validarConfirmarSenha(ilSenhaCadastro, etSenhaCadastro, ilSenhaConfirmaCadastro, etSenhaConfirmaCadastro)) {
            return;
        }
        presenter.callServiceGravarCadastro(etNomeCadastro, etEmailCadastro, etSenhaCadastro);
    }

    private void setTextChangedListener() {
        etNomeCadastro.addTextChangedListener(new ObservadorTextWatcher(etNomeCadastro));
        etEmailCadastro.addTextChangedListener(new ObservadorTextWatcher(etEmailCadastro));
        etSenhaCadastro.addTextChangedListener(new ObservadorTextWatcher(etSenhaCadastro));
        etSenhaConfirmaCadastro.addTextChangedListener(new ObservadorTextWatcher(etSenhaConfirmaCadastro));
    }

    @Override
    public void chamarIntentTelaPrincipal(Usuario usuario) {
        Intent intent = new Intent(this, ControleActivity.class);
        intent.putExtra("USUARIO", usuario);
        startActivity(intent);
    }

    @Override
    public void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    @Override
    public void showSnackbar(String mensagem) {
        Snackbar.make(getCurrentFocus(), mensagem, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void setTextInputLayout(TextInputLayout textInputLayout, String mensagem) {
        textInputLayout.setError(mensagem);
    }

    @Override
    public void setDrawableEditText(EditText editText, int left, int top, int right, int bottom) {
        editText.setCompoundDrawablesWithIntrinsicBounds(left, top, right, bottom);
    }

    @Override
    public void setErrorEnabled(TextInputLayout textInputLayout, boolean error) {
        textInputLayout.setErrorEnabled(error);
    }

    @Override
    public void usuarioValidado(Usuario usuario) {

    }

    private class ObservadorTextWatcher implements TextWatcher {
        private View view;

        private ObservadorTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.et_nome_cadastro:
                    presenter.validarNome(ilNomeCadastro, etNomeCadastro);
                    break;
                case R.id.et_email_cadastro:
                    presenter.validarEmail(ilEmailCadastro, etEmailCadastro);
                    break;
                case R.id.et_senha_cadastro:
                    presenter.validarSenha(ilSenhaCadastro, etSenhaCadastro);
                    break;
                case R.id.et_senha_confirma_cadastro:
                    presenter.validarConfirmarSenha(ilSenhaCadastro, etSenhaCadastro, ilSenhaConfirmaCadastro, etSenhaConfirmaCadastro);
                    break;
            }
        }
    }

    @Override
    public void chamaDialogEmailExistente(final Usuario usuario) {
        LayoutInflater inflater = getLayoutInflater();
        View view = inflater.inflate(R.layout.item_dialog_email_duplicado, null);
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setPositiveButton(R.string.dialog_sobreescrever, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Usuario usuario = new UsuarioDAO(getApplicationContext()).getUsuarioByEmail(etEmailCadastro.getText().toString());
                new UsuarioDAO(getApplicationContext()).deleteUsuario(usuario);
                presenter.callServiceGravarCadastro(etNomeCadastro, etEmailCadastro, etSenhaCadastro);
            }
        });

        builder.setNegativeButton(R.string.dialog_cancelar, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                alertDialog.dismiss();
            }
        });
        builder.setView(view);
        alertDialog = builder.create();
        alertDialog.show();
    }

    @Override
    public void onBackPressed() {
        final AlertDialog.Builder mBuilder = new AlertDialog.Builder(this);
        mBuilder.setTitle(R.string.title_cancelar_cadastro);
        mBuilder.setNegativeButton(R.string.dialog_cancelar, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mAlertDialog.dismiss();
            }
        });
        mBuilder.setPositiveButton(R.string.dialog_confirmar, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mAlertDialog.dismiss();
                finish();
            }
        });
        mAlertDialog = mBuilder.create();
        mAlertDialog.show();
    }
}