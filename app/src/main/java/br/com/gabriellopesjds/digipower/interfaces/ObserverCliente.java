package br.com.gabriellopesjds.digipower.interfaces;

import br.com.gabriellopesjds.digipower.domain.Usuario;

/**
 * Created by gabri on 24/02/2018.
 */

public interface ObserverCliente {
    public void chamarIntentTelaPrincipal(Usuario usuario);
}
