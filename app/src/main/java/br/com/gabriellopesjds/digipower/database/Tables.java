package br.com.gabriellopesjds.digipower.database;

public interface Tables {

    static final String TABLE_USUARIO = "usuario";
    static final String COLUMN_USUARIO_ID = "usuario_id";
    static final String COLUMN_USUARIO_NOME = "usuario_nome";
    static final String COLUMN_USUARIO_EMAIL = "usuario_email";
    static final String COLUMN_USUARIO_SENHA = "usuario_senha";
    static final String COLUMN_USUARIO_LIBERACAO = "usuario_liberacao";

}
