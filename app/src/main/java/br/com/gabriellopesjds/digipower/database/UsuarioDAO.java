package br.com.gabriellopesjds.digipower.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import br.com.gabriellopesjds.digipower.domain.Usuario;

public class UsuarioDAO implements Tables {
    private SQLiteDatabase db;
    private CriarBancoSqlite criarBancoSqlite;

    public UsuarioDAO(Context context) {
        criarBancoSqlite = new CriarBancoSqlite(context);
        db = criarBancoSqlite.getWritableDatabase();
    }

    public void addUsuario(Usuario usuario) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_USUARIO_ID, usuario.getIdCliente());
        values.put(COLUMN_USUARIO_NOME, usuario.getNome());
        values.put(COLUMN_USUARIO_EMAIL, usuario.getEmail());
        values.put(COLUMN_USUARIO_SENHA, usuario.getSenha());
        values.put(COLUMN_USUARIO_LIBERACAO, usuario.getLiberacao());

        db.insert(TABLE_USUARIO, null, values);
        db.close();
    }

    public List<Usuario> getAllUsuario() {
        // array of columns to fetch
        String[] columns = {
                COLUMN_USUARIO_ID,
                COLUMN_USUARIO_EMAIL,
                COLUMN_USUARIO_NOME,
                COLUMN_USUARIO_SENHA,
                COLUMN_USUARIO_LIBERACAO
        };
        // sorting orders
        String sortOrder =
                COLUMN_USUARIO_NOME + " ASC";
        List<Usuario> listaUsuario = new ArrayList<Usuario>();

        Cursor cursor = db.query(TABLE_USUARIO,
                columns,
                null,
                null,
                null,
                null,
                sortOrder);

        if (cursor.moveToFirst()) {
            do {
                Usuario user = new Usuario();
                user.setIdCliente(cursor.getString(cursor.getColumnIndex(COLUMN_USUARIO_ID)));
                user.setNome(cursor.getString(cursor.getColumnIndex(COLUMN_USUARIO_NOME)));
                user.setEmail(cursor.getString(cursor.getColumnIndex(COLUMN_USUARIO_EMAIL)));
                user.setSenha(cursor.getString(cursor.getColumnIndex(COLUMN_USUARIO_SENHA)));
                user.setLiberacao(cursor.getString(cursor.getColumnIndex(COLUMN_USUARIO_LIBERACAO)));
                listaUsuario.add(user);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return listaUsuario;
    }

    public Usuario getUsuarioByEmail(String email) {
        Usuario user = new Usuario();

        String[] columns = {
                COLUMN_USUARIO_ID,
                COLUMN_USUARIO_EMAIL,
                COLUMN_USUARIO_NOME,
                COLUMN_USUARIO_SENHA,
                COLUMN_USUARIO_LIBERACAO
        };

        String selectColuna = COLUMN_USUARIO_EMAIL + " = ?";

        String[] selectionArgs = {email};

        Cursor cursor = db.query(TABLE_USUARIO,
                columns,
                selectColuna,
                selectionArgs,
                null,
                null,
                null);

        if (cursor.moveToFirst()) {
            do {
                user.setIdCliente(cursor.getString(cursor.getColumnIndex(COLUMN_USUARIO_ID)));
                user.setNome(cursor.getString(cursor.getColumnIndex(COLUMN_USUARIO_NOME)));
                user.setEmail(cursor.getString(cursor.getColumnIndex(COLUMN_USUARIO_EMAIL)));
                user.setSenha(cursor.getString(cursor.getColumnIndex(COLUMN_USUARIO_SENHA)));
                user.setLiberacao(cursor.getString(cursor.getColumnIndex(COLUMN_USUARIO_LIBERACAO)));
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return user;
    }

    public void updateUsuario(Usuario user) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_USUARIO_NOME, user.getNome());
        values.put(COLUMN_USUARIO_EMAIL, user.getEmail());
        values.put(COLUMN_USUARIO_SENHA, user.getSenha());

        db.update(TABLE_USUARIO, values, COLUMN_USUARIO_ID + " = ?",
                new String[]{String.valueOf(user.getIdCliente())});
        db.close();
    }

    public void deleteUsuario(Usuario user) {
        db.delete(TABLE_USUARIO, COLUMN_USUARIO_ID + " = ?",
                new String[]{String.valueOf(user.getIdCliente())});
        db.close();
    }

    public boolean checkUsuario(String email) {
        String[] columns = {
                COLUMN_USUARIO_ID
        };

        String selectColuna = COLUMN_USUARIO_EMAIL + " = ?";

        String[] selectionArgs = {email};

        Cursor cursor = db.query(TABLE_USUARIO,
                columns,
                selectColuna,
                selectionArgs,
                null,
                null,
                null);
        int cursorCount = cursor.getCount();
        cursor.close();
        db.close();

        if (cursorCount > 0) {
            return true;
        }
        return false;
    }

    public boolean checkUsuario(String email, String senha) {
        String[] columns = {
                COLUMN_USUARIO_ID
        };

        String selectColunas = COLUMN_USUARIO_EMAIL + " = ?" + " AND " + COLUMN_USUARIO_SENHA + " = ?";

        String[] selectionArgs = {email, senha};

        Cursor cursor = db.query(TABLE_USUARIO,
                columns,
                selectColunas,
                selectionArgs,
                null,
                null,
                null);

        int cursorCount = cursor.getCount();

        cursor.close();
        db.close();
        if (cursorCount > 0) {
            return true;
        }
        return false;
    }
}