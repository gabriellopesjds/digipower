package br.com.gabriellopesjds.digipower.mvp;

import android.content.Context;

import com.facebook.login.LoginResult;

import br.com.gabriellopesjds.digipower.database.UsuarioDAO;
import br.com.gabriellopesjds.digipower.domain.Usuario;

public class PresenterMain implements MVP.PresenterMain {
    private MVP.ViewMain view;
    private MVP.ModelMain model;

    public PresenterMain(){
        model = new ModelMain( this );
    }

    @Override
    public boolean validarSeEmailJaExiste(String email) {
        if (!new UsuarioDAO(getContext()).checkUsuario(email)) {
            return false;
        }
        return true;
    }

    @Override
    public void setView(MVP.ViewMain view) {
        this.view = view;
    }

    @Override
    public Context getContext() {
        return (Context) view;
    }

    @Override
    public void fazerLoginComFacebook(LoginResult loginResult) {
        model.fazerLoginComFacebook(loginResult);
    }

    @Override
    public void chamarIntentTelaPrincipal(Usuario usuario) {
        view.chamarIntentTelaPrincipal(usuario);
    }
}
